SET DIR=%~dp0
echo %DIR%
echo %UserProfile%
rmdir /s %UserProfile%\.vim
del %UserProfile%\_vimrc
mklink %UserProfile%\_vimrc %DIR%\_vimrc
git clone https://github.com/VundleVim/Vundle.vim.git %UserProfile%/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
echo "Ready!"
