" size of a hard tabstop \t
set tabstop=2

" size of an "indent" >> or <<
set shiftwidth=2 

" a combination of spaces and tabs are used to simulate stops at a width
" other than the (hard)tabstop
set softtabstop=2

" make "tab" insert indents instead of tabs at the beginning of a line
set smarttab

" always uses spaces instead of tab characters
set expandtab

syntax on

colorscheme slate 

" to see the line numbers
set number

if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window (for an alternative on Windows, see simalt below).
  set lines=999 columns=999
endif
"
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Inconsolata\ 12
  elseif has("gui_macvim")
    set guifont=Menlo\ Regular:h14
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif

filetype plugin indent on

" =============== REGISTERS ================

" so you can edit your q macro at the end of the file
let @p='mqGo"qp'

" so you can save the edited q macro at the end of the file and clean it
let @d='G0"qd$dd`q'

" use the space to run the q macro
nnoremap <Space> @q

" Substitude word with buffer
nnoremap S "_diwP

" ===== General Mappings ======

map <C-J> <C-W>j<C-W>
map <C-K> <C-W>k<C-W>
map <C-H> <C-W>h<C-W>
map <C-L> <C-W>l<C-W>
map <C-I>j gqaj
 

" so you can use Ctrl-C to copy to the OS clipboard
vmap <C-c> "+y

" ======== Explorer =========

set nocompatible
filetype off

let g:nerdtree_tabs_open_on_console_startup=1

" set Runtime path to inc Vundle and initialize
set rtp+=$HOME/.vim/bundle/Vundle.vim

" this is the call to begin the Vundle Plugin Opperation

call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/tComment'

Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'

Plugin 'honza/vim-snippets'

Plugin 'lrvick/Conque-Shell'

Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'scrooloose/syntastic'

Plugin 'tpope/vim-jdaddy'

call vundle#end()

filetype plugin indent on
