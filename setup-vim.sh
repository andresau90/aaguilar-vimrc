DIR="$(cd "$(dirname "$0")" && pwd)"
rm -rf ~/.vim ~/.vimrc
ln "${DIR}/_vimrc" ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
echo "Ready!"
